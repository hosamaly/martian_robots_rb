require_relative '../lib/grid'

RSpec.describe Grid do
  MAX_X = Grid::MAX_X
  MAX_Y = Grid::MAX_Y

  TOO_LARGE_COORDS = {
    x: [MAX_X + 1,         1],
    y: [1,         MAX_Y + 1]
  }

  CORNERS = {
    'north east' => [MAX_X, MAX_Y],
    'north west' => [0,     MAX_Y],
    'south east' => [MAX_X,     0],
    'south west' => [0,         0],
  }

  OUTSIDE_POSITIONS = {
    north: [0,         MAX_Y + 1],
    south: [0,                -1],
    east:  [MAX_X + 1,         0],
    west:  [-1,                0],
  }

  subject(:grid) { Grid.new(MAX_X, MAX_Y) }

  TOO_LARGE_COORDS.each do |key, coords|
    it "raises an error when created with a large #{key}" do
      expect { Grid.new(*coords) }.to raise_error
    end
  end

  describe '#has_position' do
    context 'is true when' do
      specify 'position is somewhere in the grid' do
        x = MAX_X / 3
        y = MAX_Y * 2 / 3

        expect(grid).to have_position(x, y)
      end

      CORNERS.each do |corner, position|
        specify "position is at the #{corner} corner" do
          expect(grid).to have_position(*position)
        end
      end
    end

    context 'is false when' do
      OUTSIDE_POSITIONS.each do |direction, position|
        specify "position is to the #{direction} of the grid" do
          expect(grid).not_to have_position(*position)
        end
      end
    end

    it 'is true for the single point in the smallest grid' do
      expect(Grid.new(0, 0)).to have_position(0, 0)
    end
  end

  describe '#dangerous!' do
    it 'is not dangerous by default' do
      expect(grid.dangerous?(0, 0)).to be false
    end

    it 'remembers dangerous positions' do
      grid.dangerous!(1, 1)
      expect(grid.dangerous?(1, 1)).to be true
    end
  end
end
