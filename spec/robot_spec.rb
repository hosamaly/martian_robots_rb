require_relative '../lib/robot'

RSpec.shared_examples 'applies operation in each direction' do |directions_and_values|
  directions_and_values.each do |direction, target_value|
    context "when facing #{direction}" do
      subject(:robot) { Robot.new(1, 1, direction, grid) }

      it 'applies operation correctly' do
        expect {
          apply_operation
        }.to change { observed_property }.to target_value
      end
    end
  end
end

RSpec.describe Robot do
  # Where the robot should go if it moves in a particular direction, starting from [1, 1]
  MOVES = {
      north: [1, 2],
      south: [1, 0],
      east:  [2, 1],
      west:  [0, 1]
  }

  CLOCKWISE_ROTATION = {
      north: :east,
      east:  :south,
      south: :west,
      west:  :north,
  }

  ANTI_CLOCKWISE_ROTATION = {
      north: :west,
      west:  :south,
      south: :east,
      east:  :north,
  }

  # instruction => changed property
  INSTRUCTION_PROPERTIES = {
      'R' => :direction,
      'L' => :direction,
      'F' => :position,
  }

  let(:grid) { instance_double('Grid', has_position?: true, dangerous?: false) }

  subject(:robot) { Robot.new(1, 1, :north, grid) }

  it { should_not be_lost }

  describe '#move_forward' do
    def apply_operation
      subject.move_forward
    end

    def observed_property
      subject.position
    end

    include_examples 'applies operation in each direction', MOVES

    context 'if it moves off an edge' do
      before(:each) do
        expect(grid).to receive(:has_position?).and_return(false)
        allow(grid).to receive(:dangerous!)
      end

      it 'signals that it is lost' do
        expect {
          robot.move_forward
        }.to change { robot.lost? }.to true
      end

      it 'saves the last position before being lost' do
        expect {
          robot.move_forward
        }.not_to change { robot.position }
      end

      it 'flags the position as dangerous' do
        expect(grid).to receive(:dangerous!).with(1, 1)
        robot.move_forward
      end
    end

    context 'if it is in a dangerous position' do
      before(:each) do
        expect(grid).to receive(:dangerous?).and_return(true)
      end

      it 'ignores the instruction if it will cause it to fall' do
        expect(grid).to receive(:has_position?).and_return(false)
        expect {
          robot.move_forward
        }.not_to change { robot.position }
      end

      it 'processes the instruction if it would not fall' do
        expect(grid).to receive(:has_position?).and_return(true)
        expect {
          robot.move_forward
        }.to change { robot.position }
      end
    end
  end

  describe '#rotate_right' do
    def apply_operation
      subject.rotate_right
    end

    def observed_property
      subject.direction
    end

    include_examples 'applies operation in each direction', CLOCKWISE_ROTATION
  end

  describe '#rotate_left' do
    def apply_operation
      subject.rotate_left
    end

    def observed_property
      subject.direction
    end

    include_examples 'applies operation in each direction', ANTI_CLOCKWISE_ROTATION
  end

  describe '#process_instruction' do
    subject(:robot) { Robot.new(1, 1, :north, Grid.new(10, 10)) }

    INSTRUCTION_PROPERTIES.each do |instruction, observed_property|
      it "handles the #{instruction} instruction" do
        expect {
          robot.process_instruction(instruction)
        }.to change(robot, observed_property)
      end
    end

    it 'rejects unknown instructions' do
      expect {
        robot.process_instruction('XYZ')
      }.to raise_error(ArgumentError)
    end

    context 'when lost' do
      before(:each) { robot.instance_variable_set(:@lost, true) }

      INSTRUCTION_PROPERTIES.each do |instruction, observed_property|
        it "discards the #{instruction} instruction" do
          expect {
            robot.process_instruction('R')
          }.not_to change(robot, observed_property)
        end
      end
    end
  end

  describe '#str_info' do
    subject(:robot) { Robot.new(13, 27, MOVES.keys.sample, grid) }

    it 'matches the required output format' do
      info = robot.str_info
      expect(info).to match(/^\d+ \d+ [NSEW]$/)
    end

    it 'shows LOST if it is lost' do
      robot.instance_variable_set(:@lost, true)
      info = robot.str_info
      expect(info).to match(/^\d+ \d+ [NSEW] LOST$/)
    end
  end
end
