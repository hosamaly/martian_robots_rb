require_relative './grid'

class Robot
  attr_reader :x, :y, :direction

  def initialize(x, y, direction, grid)
    @x = x
    @y = y
    @direction = normalize_direction(direction)
    @lost = false
    @grid = grid
  end

  def position
    [x, y]
  end

  def lost?
    @lost
  end

  def process_instruction(instruction)
    return if lost?

    case instruction
    when 'L'
      rotate_left
    when 'R'
      rotate_right
    when 'F'
      move_forward
    else
      fail ArgumentError, "Unknown instruction #{instruction}"
    end
  end

  def move_forward
    delta = MOVE_DELTA[direction]
    new_x = @x + delta[0]
    new_y = @y + delta[1]

    # An instruction to move "off" the world from a grid point from which a robot has been
    # previously lost is simply ignored by the current robot.
    will_fall = !@grid.has_position?(new_x, new_y)
    return if @grid.dangerous?(@x, @y) && will_fall

    if will_fall
      # Lost robots leave a robot "scent" that prohibits future robots from dropping off the world
      # at the same grid point.
      @grid.dangerous!(@x, @y)
      @lost = true
    else
      @x = new_x
      @y = new_y
    end
  end

  def rotate_right
    rotate(+1)
  end

  def rotate_left
    rotate(-1)
  end

  # Returns the position and direction of the robot, and "LOST" if it's lost
  def str_info
    "#{@x} #{@y} #{@direction.to_s[0].upcase}#{@lost ? ' LOST' : ''}"
  end

  private

  # All directions, listed in clockwise order
  DIRECTIONS = [:north, :east, :south, :west]

  MOVE_DELTA = {
    north: [ 0,  1],
    south: [ 0, -1],
    east:  [ 1,  0],
    west:  [-1,  0],
  }

  def rotate(delta)
    rotated_index = DIRECTIONS.find_index(direction) + delta
    @direction = DIRECTIONS[rotated_index % DIRECTIONS.length]
  end

  def normalize_direction(direction)
    normalized = DIRECTIONS.find do |normalized_direction|
      normalized_direction.to_s.start_with?(direction.to_s.downcase)
    end

    fail ArgumentError, "Unknown direction #{direction}" if normalized.nil?

    normalized

    # case direction.to_s.downcase
    # when 'n', 'north'
    #   :north
    # when 's', 'south'
    #   :south
    # when 'e', 'east'
    #   :east
    # when 'w', 'west'
    #   :west
    # else
    #   fail ArgumentError, "Unknown direction #{direction}"
    # end
  end
end
