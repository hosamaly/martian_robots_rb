class Grid
  MAX_COORDINATE = 50
  MAX_X = MAX_COORDINATE
  MAX_Y = MAX_COORDINATE

  attr_reader :top, :right

  def initialize(right, top)
    unless right.between?(0, MAX_X) && top.between?(0, MAX_Y)
      fail ArgumentError, "[#{right}, #{top}] is not within [#{MAX_X}, #{MAX_Y}]"
    end

    @right = right
    @top = top
    @dangerous = {}
  end

  def has_position?(x, y)
    x.between?(0, right) && y.between?(0, top)
  end

  # Flags a position as dangerous
  def dangerous!(x, y)
    @dangerous[[x, y]] = true
  end

  # Checks whether a position has been flagged as dangerous
  def dangerous?(x, y)
    @dangerous[[x, y]] == true
  end
end
