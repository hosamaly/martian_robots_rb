require_relative './grid'
require_relative './robot'

class Parser
  # @param [IO] input
  def self.parse(input)
    grid_right, grid_top = input.gets.split(' ').map(&:to_i)
    grid = Grid.new(grid_right, grid_top)
    while line = input.gets
      line.strip!
      next if line.empty?

      robot_x, robot_y, robot_dir = line.split(' ')
      instructions = input.gets.strip.split('')

      robot = Robot.new(robot_x.to_i, robot_y.to_i, robot_dir, grid)
      instructions.each do |instruction|
        robot.process_instruction(instruction)
      end

      puts robot.str_info
    end
  end
end
