#!/usr/bin/env ruby
require_relative '../lib/parser'

if ARGV.length == 0
  # Read input from STDIN
  Parser.parse(STDIN)
else # ARGV.length >= 1
  # Read input from a file (or files)
  ARGV.each do |file_name|
    input = File.new(file_name, 'r')
    Parser.parse(input)
  end
end