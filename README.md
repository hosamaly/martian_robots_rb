This is a solution to the "Martian Robots" problem, focusing on simplicity and Test Driven Development (TDD).

This repository contains a Ruby project.

Run `bundle install` to install the application's dependencies.

To run the application on the command line, run `bundle exec script/runner.rb`.
If run without arguments, the application reads input STDIN. If supplied with
arguments, it treats each of them as a file name, and processes each one independently.

To run the tests, simply run `bundle exec rspec`. Coverage info will be collected automatically.
At the time of writing this, the code has a 100% coverage at an average of 8 hits/line.